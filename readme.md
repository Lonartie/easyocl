*Documentation for 'EasyOCL' � by Leon Gierschner 16.01.2020*

# Compilation:

-----------------------------------------------------------------------------------------------------------------------------

- To compile this your program with this extension you need to have Qt installed (because I used Qt
types such as QString) but only the Qt5Core.dll / .lib / headers are nessecary.
- There is no need to have any OpenCL sdk installed because this library was statically linked with one.
But you have to make sure there are drivers installed on you system that support OpenCL, otherwise this library will
not find the device you may want to use. Even a fallback on an emulated device can occur...

# What does this extension?:

-----------------------------------------------------------------------------------------------------------------------------

- blablabla

# Usage:

-----------------------------------------------------------------------------------------------------------------------------

- blabliblub
