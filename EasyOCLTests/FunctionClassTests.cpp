//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>
#include "../EasyOCL/EasyOCL"

using namespace EasyOCL;
class math2: public FunctionClass
{
	FUNCTION_CLASS_H(math2);
public: FUNCTION_H(sub, float, (const float*, a, const float*, b));
};
FUNCTION_CLASS_CPP_BEGIN(math2);
FUNCTION_CPP(math2, sub, float, (const float*, a, const float*, b), {return a[get_global_id(0)] - b[get_global_id(0)];});

class math: public FunctionClass
{
	FUNCTION_CLASS_H(math);
public: FUNCTION_H(add, float, (const float*, a, float*, b));
};
FUNCTION_CLASS_CPP_BEGIN(math);
FUNCTION_CPP(math, add, float, (const float*, a, float*, b), {return a[get_global_id(0)] + b[get_global_id(0)]; });

class math_test: public KernelClass
{
	KERNEL_CLASS_H(math_test);
public: KERNEL_FUNCTION_H(add_lists, (const float*, a, float*, b));
};
KERNEL_CLASS_CPP_BEGIN(math_test);
KERNEL_FUNCTION_CPP(math_test, add_lists, (const float*, a, float*, b), {b[get_global_id(0)] = math2::sub(a, b) + math::add(a, b); });
KERNEL_FUNCTION_USE_CLASSES(math_test, add_lists, math, math2);

KERNEL_CLASS_CPP_END();

BOOST_AUTO_TEST_SUITE(TS_FunctionClass)

BOOST_AUTO_TEST_CASE(FunctionClassCanBeCreated)
{
	BOOST_CHECK_EQUAL(true, math_test::compile("add_lists", OCLDevice::CPU()));
	auto& prgm = math_test::getProgram(&math_test::add_lists, OCLDevice::CPU());
	BOOST_CHECK(ErrorType::NO_CL_ERROR, prgm.errorType);
	std::vector<float> res = {1, 2, 3};
	auto result = math_test().add_lists(OCLDevice::CPU(), 3, std::vector<float> {1, 2, 3}, res);
	BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

	BOOST_CHECK_EQUAL(2, res[0]);
	BOOST_CHECK_EQUAL(4, res[1]);
	BOOST_CHECK_EQUAL(6, res[2]);
}

BOOST_AUTO_TEST_SUITE_END()