//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

#include <crtdbg.h>
#include <boost/test/unit_test.hpp>

#define BOOST_AUTO_TEST_CASE_MEMORY(name)\
    void my_test_cases__##name();       \
    BOOST_AUTO_TEST_CASE(name)          \
    {                                   \
        mytests::heap heap_scope;       \
        my_test_cases__##name();        \
        (void)heap_scope;               \
    }                                   \
    void my_test_cases__##name()

namespace mytests
{
   class heap
   {
#   if !defined(NDEBUG) && defined(_MSC_VER)
   public:
      heap()
      {
         _CrtMemCheckpoint(&oldState);
      }

      ~heap()
      {
         _CrtMemState curState, diffState;

         _CrtMemCheckpoint(&curState);

         int leaked_memory = _CrtMemDifference(&diffState, &oldState, &curState);

         BOOST_CHECK_EQUAL(0, leaked_memory);
      }

   private:
      _CrtMemState oldState;
#   endif
   };
}