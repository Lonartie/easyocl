//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>
#include "../EasyOCL/EasyOCL"

using namespace EasyOCL;

// HEADER

class Calculator: public KernelClass
{
	KERNEL_CLASS_H(Calculator);

public:

	Calculator()
	{}

	template<typename ...ARGS>
	Calculator(EasyOCL::ExecutionResult(Calculator::*fn)(ARGS...), OCLDevice device)
	{
		compile<Calculator, ARGS...>(fn, device);
	}

	KERNEL_FUNCTION_H(calculate, (int*, list));
	KERNEL_FUNCTION_H(calculate2, (int*, list, const int*, input));
	KERNEL_FUNCTION_H(calculate3, (int*, list));
};


// CPP

KERNEL_CLASS_CPP_BEGIN(Calculator);
KERNEL_FUNCTION_CPP(Calculator, calculate, (int*, list),
						  {
							  list[get_global_id(0)] = 1;
						  });

KERNEL_FUNCTION_CPP(Calculator, calculate2, (int*, list, const int*, input),
						  {
							  list[get_global_id(0)] = input[get_global_id(0)] * 2;
						  });

KERNEL_FUNCTION_CPP(Calculator, calculate3, (int*, list),
						  {
							  list[get_global_id(0)] = 2;
						  });

KERNEL_CLASS_CPP_END();


// TESTS

BOOST_AUTO_TEST_SUITE(TS_ClassMacros);

static bool CAT(__compiled, __LINE__) = compile<Calculator>(&Calculator::calculate, OCLDevice::CPU());
static bool CAT(__compiled, __LINE__) = compile<Calculator>(&Calculator::calculate2, OCLDevice::CPU());
static bool CAT(__compiled, __LINE__) = compile<Calculator>(&Calculator::calculate3, OCLDevice::CPU());

BOOST_AUTO_TEST_CASE(OclFunctionRegistration)
{
	Calculator();
	auto& map = Calculator::__class_programs;
	BOOST_CHECK_EQUAL(3, map.size());
}

BOOST_AUTO_TEST_CASE(OclFunctionRunning_1)
{
	std::vector<int32_t> res = {0,0,0,0};
	auto result = Calculator().calculate(OCLDevice::CPU(), res.size(), res);

	BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

	BOOST_CHECK_EQUAL(1, res[0]);
	BOOST_CHECK_EQUAL(1, res[1]);
	BOOST_CHECK_EQUAL(1, res[2]);
	BOOST_CHECK_EQUAL(1, res[3]);
}

BOOST_AUTO_TEST_CASE(OclFunctionRunning_2)
{
	std::vector<int32_t> res = {0,0,0,0};
	const std::vector<int32_t> dat = {1,2,3,4};
	auto result = Calculator().calculate2(OCLDevice::CPU(), res.size(), res, dat);

	BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

	BOOST_CHECK_EQUAL(2, res[0]);
	BOOST_CHECK_EQUAL(4, res[1]);
	BOOST_CHECK_EQUAL(6, res[2]);
	BOOST_CHECK_EQUAL(8, res[3]);
}

BOOST_AUTO_TEST_CASE(OclFunctionRunning_3)
{
	std::vector<int32_t> res = {0,0,0,0};
	auto result = Calculator().calculate3(OCLDevice::CPU(), res.size(), res);

	BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

	BOOST_CHECK_EQUAL(2, res[0]);
	BOOST_CHECK_EQUAL(2, res[1]);
	BOOST_CHECK_EQUAL(2, res[2]);
	BOOST_CHECK_EQUAL(2, res[3]);
}

std::size_t measure(std::function<void()> fn)
{
	auto begin = std::chrono::system_clock::now();
	fn();
	auto end = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
}

BOOST_AUTO_TEST_CASE(OclFunctionTimer)
{
	std::vector<int32_t> res = {0,0,0,0};
	
	auto resu = measure([&]() { Calculator::calculate(OCLDevice::CPU(), res.size(), res); });

	BOOST_TEST_MESSAGE("calculation time: " << resu);
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END();