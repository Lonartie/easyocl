//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>
#include "../EasyOCL/EasyOCL"

using namespace EasyOCL;

BOOST_AUTO_TEST_SUITE(TS_DevicesInformation);

BOOST_AUTO_TEST_CASE(ListAllDevices)
{
   BOOST_TEST_MESSAGE("default cpu: " << OCLDevice::CPU().getName().toStdString());
   BOOST_TEST_MESSAGE("default gpu: " << OCLDevice::GPU().getName().toStdString());
   BOOST_TEST_MESSAGE("---------------------------------------");

	for (const auto& device : OCLDevice::getDevices())
	{
		QString vendor = device.getVendor() == Intel ? "Intel" : device.getVendor() == NVidia ? "NVidia" : device.getVendor() == AMD ? "AMD" : "Unknown";
      QString type = device.getDeviceType() == CPU ? "CPU" : device.getDeviceType() == GPU ? "GPU" : "Unknown";
      BOOST_TEST_MESSAGE("found device: " << device.getName().toStdString());
      BOOST_TEST_MESSAGE("type: " << type.toStdString());
      BOOST_TEST_MESSAGE("vendor: " << vendor.toStdString());
      BOOST_TEST_MESSAGE("driver: " << device.getDriverVersion().toStdString());
      BOOST_TEST_MESSAGE("clock freq: " << device.getClockFrequency());
      BOOST_TEST_MESSAGE("work groups: " << device.getWorkGroupSize());
      BOOST_TEST_MESSAGE("memory: " << device.getAvailableMemory());
      BOOST_TEST_MESSAGE("compute units: " << device.getComputeUnits());
		BOOST_TEST_MESSAGE("---------------------------------------");
	}

	BOOST_CHECK_EQUAL(true, true);
}


BOOST_AUTO_TEST_SUITE_END();