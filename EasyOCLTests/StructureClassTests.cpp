#include <boost/test/unit_test.hpp>
#include "../EasyOCL/EasyOCL"
using namespace EasyOCL;

namespace test
{
   struct Vector: StructureClass
   {
      STRUCTURE_CLASS_H(Vector);

      Vector() = default;
      Vector(float x, float y): x(x), y(y) {};

      STRUCTURE_FIELD_H(float, x);
      STRUCTURE_FIELD_H(float, y);
   };
   STRUCTURE_CLASS_CPP_BEGIN(Vector);
   STRUCTURE_FIELD_CPP(Vector, float, x);
   STRUCTURE_FIELD_CPP(Vector, float, y);
   STRUCTURE_CLASS_CPP_END();

   struct Vector3: StructureClass
   {
      STRUCTURE_CLASS_H(Vector3);

      Vector3() = default;
      Vector3(Vector x, Vector y, Vector z): x(x), y(y), z(z) {};

      STRUCTURE_FIELD_H(Vector, x);
      STRUCTURE_FIELD_H(Vector, y);
      STRUCTURE_FIELD_H(Vector, z);
   };

   STRUCTURE_CLASS_CPP_BEGIN(Vector3);
   STRUCTURE_FIELD_CPP(Vector3, struct Vector, x);
   STRUCTURE_FIELD_CPP(Vector3, struct Vector, y);
   STRUCTURE_FIELD_CPP(Vector3, struct Vector, z);
   STRUCTURE_CLASS_CPP_END();


   class _math: KernelClass
   {
      KERNEL_CLASS_H(_math);
   public:
      KERNEL_FUNCTION_H(mul_2, (const struct Vector3*, in, struct Vector3*, out));
   };
   KERNEL_CLASS_CPP_BEGIN(_math);
   KERNEL_FUNCTION_CPP(_math, mul_2, (const struct Vector3*, in, struct Vector3*, out),
                       {
                          struct Vector3 res = in[get_global_id(0)];

                          struct Vector _x = res.x;
                          struct Vector _y = res.y;
                          struct Vector _z = res.z;

                          _x.x *= 2;
                          _x.y *= 2;
                          _y.x *= 2;
                          _y.y *= 2;
                          _z.x *= 2;
                          _z.y *= 2;

                          res.x = _x;
                          res.y = _y;
                          res.z = _z;

                          out[get_global_id(0)] = res;
                       }, Vector3, Vector);
   KERNEL_CLASS_CPP_END();
}
BOOST_AUTO_TEST_SUITE(TS_StructureClass);

BOOST_AUTO_TEST_CASE(CanDefineStructureClass)
{
   using namespace test;
   BOOST_CHECK_EQUAL(0, Vector().x);
   BOOST_CHECK_EQUAL(true, Vector::getAll().contains("float x;"));
   BOOST_CHECK_EQUAL(true, Vector::getAll().contains("float y;"));
   BOOST_CHECK_EQUAL(true, _math::compile(&_math::mul_2, OCLDevice::CPU()));
   std::vector<Vector3> list = { Vector3(Vector(1, 2), Vector(3, 4), Vector(5, 6)) };

   auto& prgm = _math::getProgram("mul_2", OCLDevice::CPU());

   auto result = _math::mul_2(OCLDevice::CPU(), 2, list, list);

   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);
   BOOST_CHECK_EQUAL(2, list[0].x.x);
   BOOST_CHECK_EQUAL(4, list[0].x.y);
   BOOST_CHECK_EQUAL(6, list[0].y.x);
   BOOST_CHECK_EQUAL(8, list[0].y.y);
   BOOST_CHECK_EQUAL(10, list[0].z.x);
   BOOST_CHECK_EQUAL(12, list[0].z.y);
}

BOOST_AUTO_TEST_SUITE_END();