//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>

#include "main.h"
#include "VectorCalculator.h"

#include "../EasyOCL/VectorCalculator.h"

using namespace EasyOCL;

// TESTS

static std::size_t measure(std::function<void()> fn)
{
   using namespace std::chrono;
   auto begin = system_clock::now();
   fn();
   auto end = system_clock::now();
   return duration_cast<milliseconds>(end - begin).count();
}

BOOST_AUTO_TEST_SUITE(TS_VectorCalulationTests);

BOOST_AUTO_TEST_CASE(Addition)
{
   std::vector<float> a = {1, 2, 3, 4};
   std::vector<float> b = {1, 2, 3, 4};
   std::vector<float> r = {9, 9, 9, 9};
   
   auto result = VectorCalculator::add(OCLDevice::CPU(), 4, a, b, r);

   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

   BOOST_CHECK_EQUAL(2, r[0]);
   BOOST_CHECK_EQUAL(4, r[1]);
   BOOST_CHECK_EQUAL(6, r[2]);
   BOOST_CHECK_EQUAL(8, r[3]);
}

BOOST_AUTO_TEST_CASE(AdditionAsync)
{
   std::vector<float> a = {1, 2, 3, 4};
   std::vector<float> b = {1, 2, 3, 4};
   std::vector<float> r = {9, 9, 9, 9};
   volatile bool done = false;
   ErrorType tp = ErrorType::OTHER_ERROR;
   
   VectorCalculator::addAsync(OCLDevice::CPU(), 4, a, b, r, [&](ExecutionResult result) { done = true; tp = result.errorType; }).join();

   BOOST_CHECK_EQUAL(true, done);
   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, tp);
   BOOST_CHECK_EQUAL(2, r[0]);
   BOOST_CHECK_EQUAL(4, r[1]);
   BOOST_CHECK_EQUAL(6, r[2]);
   BOOST_CHECK_EQUAL(8, r[3]);
}

BOOST_AUTO_TEST_CASE(Subtraction)
{
   std::vector<float> a = {1, 2, 3, 4};
   std::vector<float> b = {1, 2, 3, 4};
   std::vector<float> r = {9, 9, 9, 9};

   auto result = VectorCalculator::sub(OCLDevice::CPU(), 4, a, b, r);

   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

   BOOST_CHECK_EQUAL(0, r[0]);
   BOOST_CHECK_EQUAL(0, r[1]);
   BOOST_CHECK_EQUAL(0, r[2]);
   BOOST_CHECK_EQUAL(0, r[3]);
}

BOOST_AUTO_TEST_CASE(SubtractionAsync)
{
   std::vector<float> a = {1, 2, 3, 4};
   std::vector<float> b = {1, 2, 3, 4};
   std::vector<float> r = {9, 9, 9, 9};
   volatile bool done = false;
   ErrorType tp = ErrorType::OTHER_ERROR;

   VectorCalculator::subAsync(OCLDevice::CPU(), 4, a, b, r, [&](ExecutionResult result) { done = true; tp = result.errorType; }).join();

   BOOST_CHECK_EQUAL(true, done);
   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, tp);
   BOOST_CHECK_EQUAL(0, r[0]);
   BOOST_CHECK_EQUAL(0, r[1]);
   BOOST_CHECK_EQUAL(0, r[2]);
   BOOST_CHECK_EQUAL(0, r[3]);
}

BOOST_AUTO_TEST_CASE(Multiplication)
{
	std::vector<float> a = {1, 2, 3, 4};
	std::vector<float> b = {	  3	 };
	std::vector<float> r = {9, 9, 9, 9};

	auto result = VectorCalculator::mul_single(OCLDevice::CPU(), 4, a, b, r);

	BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

	BOOST_CHECK_EQUAL( 3, r[0]);
	BOOST_CHECK_EQUAL( 6, r[1]);
	BOOST_CHECK_EQUAL( 9, r[2]);
	BOOST_CHECK_EQUAL(12, r[3]);
}

BOOST_AUTO_TEST_CASE(MultiplicationAsync)
{
   std::vector<float> a = {1, 2, 3, 4};
	std::vector<float> b = {	  3	 };
   std::vector<float> r = {9, 9, 9, 9};
   volatile bool done = false;
   ErrorType tp = ErrorType::OTHER_ERROR;

   VectorCalculator::mul_singleAsync(OCLDevice::CPU(), 4, a, b, r, [&](ExecutionResult result) { done = true; tp = result.errorType; }).join();

   BOOST_CHECK_EQUAL(true, done);
   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, tp);
	BOOST_CHECK_EQUAL( 3, r[0]);
	BOOST_CHECK_EQUAL( 6, r[1]);
	BOOST_CHECK_EQUAL( 9, r[2]);
	BOOST_CHECK_EQUAL(12, r[3]);
}

BOOST_AUTO_TEST_CASE(Division)
{
	std::vector<float> a = {1, 2, 3, 4};
	std::vector<float> b = {	  2	 };
	std::vector<float> r = {9, 9, 9, 9};

	auto result = VectorCalculator::div_single(OCLDevice::CPU(), 4, a, b, r);

	BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);

	BOOST_CHECK_EQUAL(0.5f, r[0]);
	BOOST_CHECK_EQUAL(1.0f, r[1]);
	BOOST_CHECK_EQUAL(1.5f, r[2]);
	BOOST_CHECK_EQUAL(2.0f, r[3]);
}

BOOST_AUTO_TEST_CASE(DivisionAsync)
{
   std::vector<float> a = {1, 2, 3, 4};
	std::vector<float> b = {	  2	 };
   std::vector<float> r = {9, 9, 9, 9};
   volatile bool done = false;
   ErrorType tp = ErrorType::OTHER_ERROR;

   VectorCalculator::div_singleAsync(OCLDevice::CPU(), 4, a, b, r, [&](ExecutionResult result) { done = true; tp = result.errorType; }).join();

   BOOST_CHECK_EQUAL(true, done);
   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, tp);
   BOOST_CHECK_EQUAL(0.5f, r[0]);
   BOOST_CHECK_EQUAL(1.0f, r[1]);
   BOOST_CHECK_EQUAL(1.5f, r[2]);
   BOOST_CHECK_EQUAL(2.0f, r[3]);
}

BOOST_AUTO_TEST_SUITE_END();