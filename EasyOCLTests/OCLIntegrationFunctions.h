//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

#include "../EasyOCL/EasyOCL"

class GetGlobalID: public EasyOCL::KernelClass
{
	KERNEL_CLASS_H(GetGlobalID);

public:

	GetGlobalID();

	static EasyOCL::ExecutionResult TEST();

	KERNEL_FUNCTION_H(GlobalIDWorks, (int*, list));
};