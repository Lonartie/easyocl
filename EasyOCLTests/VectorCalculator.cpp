//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "VectorCalculator.h"

KERNEL_CLASS_CPP_BEGIN(VectorCalculator);
using namespace EasyOCL;

KERNEL_FUNCTION_CPP(VectorCalculator, add, (const float*, a, const float*, b, float*, result),
{
   const int id = get_global_id(0);
   result[id] = a[id] + b[id];
});

KERNEL_FUNCTION_CPP(VectorCalculator, sub, (const float*, a, const float*, b, float*, result),
{
   const int id = get_global_id(0);
   result[id] = a[id] - b[id];
});

KERNEL_FUNCTION_CPP(VectorCalculator, mul_single, (const float*, a, const float*, b, float*, result),
{
   const int id = get_global_id(0);
   result[id] = a[id] * b[0];
});

KERNEL_FUNCTION_CPP(VectorCalculator, div_single, (const float*, a, const float*, b, float*, result),
{
   const int id = get_global_id(0);
   result[id] = a[id] / b[0];
});

KERNEL_FUNCTION_CPP(VectorCalculator, mag, (const float*, a, int*, size, float*, tmp, float*, result),
{
   const int id = get_global_id(0);

   //TODO: do something here

});

KERNEL_CLASS_CPP_END();


namespace compilation
{
	static auto CAT(__compiled, __LINE__) = compile<VectorCalculator>(&VectorCalculator::add, OCLDevice::CPU());
	static auto CAT(__compiled, __LINE__) = compile<VectorCalculator>(&VectorCalculator::sub, OCLDevice::CPU());
	static auto CAT(__compiled, __LINE__) = compile<VectorCalculator>(&VectorCalculator::mul_single, OCLDevice::CPU());
	static auto CAT(__compiled, __LINE__) = compile<VectorCalculator>(&VectorCalculator::div_single, OCLDevice::CPU());
}