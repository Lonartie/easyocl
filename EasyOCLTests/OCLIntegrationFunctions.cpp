//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "OCLIntegrationFunctions.h"

using namespace EasyOCL;

KERNEL_CLASS_CPP_BEGIN(GetGlobalID);

GetGlobalID::GetGlobalID()
{}

ExecutionResult GetGlobalID::TEST()
{ return {}; }

KERNEL_FUNCTION_CPP(GetGlobalID, GlobalIDWorks, (int*, list),
						  {
							  list[get_global_id(0)] = get_global_id(0);
						  });

auto globalidworks_compiled = compile<GetGlobalID>(&GetGlobalID::GlobalIDWorks, OCLDevice::CPU());

KERNEL_CLASS_CPP_END();