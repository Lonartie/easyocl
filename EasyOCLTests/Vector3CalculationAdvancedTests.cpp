#include <boost/test/unit_test.hpp>
#include "../EasyOCL/EasyOCL"

using namespace EasyOCL;

/****************/
/* Vector Class */
/****************/

struct Vector: StructureClass
{
   STRUCTURE_CLASS_H(Vector);
   STRUCTURE_FIELD_H(float, x);
   STRUCTURE_FIELD_H(float, y);
   STRUCTURE_FIELD_H(float, _pi);

   Vector() = default;
   Vector(float x, float y): x(x), y(y) {};
};
STRUCTURE_CLASS_CPP_BEGIN(Vector);
STRUCTURE_FIELD_CPP(Vector, float, x);
STRUCTURE_FIELD_CPP(Vector, float, y);
STRUCTURE_FIELD_CPP(Vector, float, _pi);
STRUCTURE_CLASS_CPP_END();

/*****************/
/* Vector3 Class */
/*****************/

struct Vector3: StructureClass
{
   STRUCTURE_CLASS_H(Vector3);
   STRUCTURE_FIELD_H(struct Vector, x);
   STRUCTURE_FIELD_H(struct Vector, y);
   STRUCTURE_FIELD_H(struct Vector, z);

   Vector3() = default;
   Vector3(Vector x, Vector y, Vector z): x(x), y(y), z(z) {};
};
STRUCTURE_CLASS_CPP_BEGIN(Vector3);
STRUCTURE_FIELD_CPP(Vector3, struct Vector, x);
STRUCTURE_FIELD_CPP(Vector3, struct Vector, y);
STRUCTURE_FIELD_CPP(Vector3, struct Vector, z);
STRUCTURE_CLASS_CPP_END();

/*********************/
/* Vector3 Functions */
/*********************/

class Vector3Functions: FunctionClass
{
   FUNCTION_CLASS_H(Vector3Functions);
public:
	FUNCTION_H(add_vector, struct Vector, (const struct Vector, a, const struct Vector, b));
	FUNCTION_H(add_vector3, struct Vector3, (const struct Vector3*, a, const struct Vector3*, b));
};
FUNCTION_CLASS_CPP_BEGIN(Vector3Functions);
EXTERN_MACRO_CONSTANT(PI, double);
EXTERN_MACRO_FUNCTION(__PI, (x));
// intentionally reversed function order!
FUNCTION_CPP(Vector3Functions, add_vector3, struct Vector3, (const struct Vector3*, a, const struct Vector3*, b),
{
   struct Vector3 res = a[get_global_id(0)];
   struct Vector3 tmp = b[get_global_id(0)];

   struct Vector _x = add_vector(res.x, tmp.x);
   struct Vector _y = add_vector(res.y, tmp.y);
   struct Vector _z = add_vector(res.z, tmp.z);

   _x._pi = PI;
   _y._pi = PI;
   _z._pi = __PI(2);

   res.x = _x;
   res.y = _y;
   res.z = _z;

   return res;
});
FUNCTION_CPP(Vector3Functions, add_vector, struct Vector, (const struct Vector, a, const struct Vector, b),
{
	struct Vector result;
	result.x = a.x + b.x;
	result.y = a.y + b.y;
	return result;
});
FUNCTION_CLASS_CPP_END();

/****************/
/* Kernel Class */
/****************/

class Vector3Calculation: KernelClass
{
   KERNEL_CLASS_H(Vector3Calculation);
public:
   KERNEL_FUNCTION_H(add, (const struct Vector3*, a, const struct Vector3*, b, struct Vector3*, out));
};
KERNEL_CLASS_CPP_BEGIN(Vector3Calculation);
KERNEL_FUNCTION_CPP(Vector3Calculation, add, (const struct Vector3*, a, const struct Vector3*, b, struct Vector3*, out),
{
   out[get_global_id(0)] = Vector3Functions::add_vector3(a, b);
});
KERNEL_FUNCTION_USE_CLASSES(Vector3Calculation, add,
                            Vector3Functions /*kernel needs vector3functions*/,
                            Vector3 /*vector3 functions needs vector3*/,
                            Vector /*vector3 needs vector*/);
KERNEL_REGISTER_MACRO_CONSTANT(Vector3Calculation, add, PI, 3.14159265, double);
KERNEL_REGISTER_MACRO_FUNCTION(Vector3Calculation, add, __PI, 3.14159265*x, (x));
KERNEL_CLASS_CPP_END();

BOOST_AUTO_TEST_SUITE(TS_Vector3CalculationAdvanced);

BOOST_AUTO_TEST_CASE(EverythingTogetherWorks)
{
   BOOST_REQUIRE(Vector3Calculation::compile("add", OCLDevice::CPU()));
   auto& prgm = Vector3Calculation::getProgram("add", OCLDevice::CPU());
   std::vector<struct Vector3> list1 =
   {
      struct Vector3
      (
         struct Vector(1, 2),
         struct Vector(3, 4),
         struct Vector(5, 6)
      )
   };
   std::vector<struct Vector3> list2 =
   {
      struct Vector3
      (
         struct Vector(12, 11),
         struct Vector(10, 9),
         struct Vector(8, 7)
      )
   };
   std::vector<struct Vector3> list3 =
   {
      struct Vector3
      (
         struct Vector(0, 0),
         struct Vector(0, 0),
         struct Vector(0, 0)
      )
   };
   std::vector<struct Vector3> out =
   {
      struct Vector3
      (
         struct Vector(13, 13),
         struct Vector(13, 13),
         struct Vector(13, 13)
      )
   };

   volatile bool done = false;
   ErrorClass result;
   result.errorType = ErrorType::OTHER_ERROR;
   Vector3Calculation::addAsync(OCLDevice::CPU(), 1, list1, list2, list3, [&](auto& res) { done = true; result = res; }).detach();

   BOOST_CHECK_EQUAL(false, done);
   std::this_thread::sleep_for(std::chrono::milliseconds(100));
   BOOST_CHECK_EQUAL(true, done);

   BOOST_CHECK_EQUAL(ErrorType::NO_CL_ERROR, result.errorType);
   BOOST_CHECK_EQUAL(out[0].x.x, list3[0].x.x);
   BOOST_CHECK_EQUAL(out[0].x.y, list3[0].x.y);
   BOOST_CHECK_EQUAL(static_cast<float>(PI), static_cast<float>(list3[0].x._pi));
   BOOST_CHECK_EQUAL(out[0].y.x, list3[0].y.x);
   BOOST_CHECK_EQUAL(out[0].y.y, list3[0].y.y);
   BOOST_CHECK_EQUAL(static_cast<float>(PI), static_cast<float>(list3[0].y._pi));
   BOOST_CHECK_EQUAL(out[0].z.x, list3[0].z.x);
   BOOST_CHECK_EQUAL(out[0].z.y, list3[0].z.y);
   BOOST_CHECK_EQUAL(__PI(2), list3[0].z._pi);
}

BOOST_AUTO_TEST_CASE(PrintKernelCode)
{
	BOOST_REQUIRE(Vector3Calculation::compile("add", OCLDevice::CPU()));
	BOOST_TEST_MESSAGE(Vector3Calculation::getProgram("add", OCLDevice::CPU()).kernelCode.toStdString());
}

BOOST_AUTO_TEST_SUITE_END();