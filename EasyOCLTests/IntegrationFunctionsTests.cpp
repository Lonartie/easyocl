//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include <boost/test/unit_test.hpp>
#include "../EasyOCL/IntegrationFunctions.h"
#include "OCLIntegrationFunctions.h"

using namespace EasyOCL;

extern bool globalidworks_compiled;

BOOST_AUTO_TEST_SUITE(TS_IntegrationFunctions);

BOOST_AUTO_TEST_CASE(GetGlobalID_CPP)
{
	BOOST_CHECK_THROW(get_global_id(0), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(GetGlobalID_OCL)
{
	BOOST_CHECK_EQUAL(false, GetGlobalID::getKernelCode(&GetGlobalID::GlobalIDWorks).isEmpty());
	BOOST_CHECK_EQUAL(true, GetGlobalID::getKernelCode(&GetGlobalID::TEST).isEmpty());
	BOOST_CHECK_EQUAL(true, globalidworks_compiled);

	GetGlobalID id;
	std::vector<int32_t> list = {0,0,0,0};
	id.GlobalIDWorks(OCLDevice::CPU(), list.size(), list);

	BOOST_CHECK_EQUAL(0, list[0]);
	BOOST_CHECK_EQUAL(1, list[1]);
	BOOST_CHECK_EQUAL(2, list[2]);
	BOOST_CHECK_EQUAL(3, list[3]);
}

BOOST_AUTO_TEST_SUITE_END();