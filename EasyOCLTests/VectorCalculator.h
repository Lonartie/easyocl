//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

#include "../EasyOCL/EasyOCL"

class VectorCalculator: public EasyOCL::KernelClass
{
   KERNEL_CLASS_H(VectorCalculator);

public:

	VectorCalculator() = default;

   template<typename ...ARGS>
   VectorCalculator(EasyOCL::ExecutionResult(* fn)(ARGS...), EasyOCL::OCLDevice device)
   {
      compile(fn, device);
   }

   KERNEL_FUNCTION_H(add, (const float*, a, const float*, b, float*, result));
   KERNEL_FUNCTION_H(sub, (const float*, a, const float*, b, float*, result));
   KERNEL_FUNCTION_H(mul_single, (const float*, a, const float*, b, float*, result));
   KERNEL_FUNCTION_H(div_single, (const float*, a, const float*, b, float*, result));
   KERNEL_FUNCTION_H(mag, (const float*, a, int*, size, float*, tmp, float*, result));
};