//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "OCLProgram.h"
#include "DNP_Executable.h"
#include "DNP_ExecutionEngine.h"
#include "FunctionalMacros.h"

#include <QtCore/QDebug>

#define EXEC (*static_cast<DNP_Executable*>(m_executable))
#define EXECP (static_cast<DNP_Executable*>(m_executable))

EasyOCL::OCLProgram::OCLProgram(void* executable)
	: m_executable(executable)
{
	error = EXEC.error;
	errorLocation = EXEC.errorLocation;
	errorType = EXEC.errorType;
}

EasyOCL::OCLProgram::OCLProgram(OCLProgram&& o)
	: m_executable(new DNP_Executable(*static_cast<const DNP_Executable*>(o.m_executable)))
	, function(o.function)
	, kernelCode(o.kernelCode)
	, device(o.device)
   , programLocation(o.programLocation)
   , macroString(o.macroString)
{
	error = EXEC.error;
	errorLocation = EXEC.errorLocation;
	errorType = EXEC.errorType;
}

EasyOCL::OCLProgram::OCLProgram(const OCLProgram& o)
	: m_executable(new DNP_Executable(*static_cast<const DNP_Executable*>(o.m_executable)))
	, function(o.function)
	, kernelCode(o.kernelCode)
   , device(o.device)
   , programLocation(o.programLocation)
   , macroString(o.macroString)
{
	error = EXEC.error;
	errorLocation = EXEC.errorLocation;
	errorType = EXEC.errorType;
}

EasyOCL::OCLProgram::~OCLProgram()
{
	DNP_Executable* ptr = EXECP;
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
		m_executable = nullptr;
	}
}

EasyOCL::OCLProgram& EasyOCL::OCLProgram::operator=(OCLProgram&& o)
{
	DNP_Executable* ptr = EXECP;
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
		m_executable = nullptr;
	}

	m_executable = new DNP_Executable(*static_cast<const DNP_Executable*>(o.m_executable));
	function = o.function;
	kernelCode = o.kernelCode;
	device = o.device;
	error = EXEC.error;
	errorLocation = EXEC.errorLocation;
	errorType = EXEC.errorType;
	programLocation = o.programLocation;
	macroString = o.macroString;
	return *this;
}

EasyOCL::OCLProgram& EasyOCL::OCLProgram::operator=(const OCLProgram& o)
{
	DNP_Executable* ptr = EXECP;
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
		m_executable = nullptr;
	}

	m_executable = new DNP_Executable(*static_cast<const DNP_Executable*>(o.m_executable));
	function = o.function;
	kernelCode = o.kernelCode;
	device = o.device;
	error = EXEC.error;
	errorLocation = EXEC.errorLocation;
   errorType = EXEC.errorType;
   programLocation = o.programLocation;
   macroString = o.macroString;
	return *this;
}

EasyOCL::ExecutionResult EasyOCL::OCLProgram::execute(std::size_t iterations, ExecutionParameters& params)
{
	if (!m_executable)
	{
		ExecutionResult result;
		result.error = "Executable was nullptr";
		result.errorType = ErrorType::EXECUTION_ERROR;
		result.errorLocation = OCL_CURRENT_LOCATION;
		return result;
	}


	EXEC.iterations = iterations;
	return DNP_ExecutionEngine::execute(EXEC, params);
}

bool EasyOCL::OCLProgram::isValid() const
{
	return errorType == ErrorType::NO_CL_ERROR;
}
