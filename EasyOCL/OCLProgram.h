//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "ErrorClass.h"
#include "ExecutionParameters.h"
#include "ExecutionResult.h"
#include "OCLDevice.h"

namespace EasyOCL
{
	class EASYOCL_EXPORT OCLProgram: public ErrorClass
	{
	public:

		OCLProgram() = default;
		OCLProgram(void* executable);
		OCLProgram(const OCLProgram& o);
		OCLProgram(OCLProgram&& o);
		~OCLProgram();

		OCLProgram& operator=(const OCLProgram& o);
		OCLProgram& operator=(OCLProgram&& o);

		template<typename ...ARGS>
		EasyOCL::ExecutionResult operator()(std::size_t iterations, ARGS...args)
		{
			auto params = toParams<ARGS...>(args...);
			std::reverse(params.begin(), params.end());
			return execute(iterations, params);
		};

		EasyOCL::ExecutionResult execute(std::size_t iterations, ExecutionParameters& params);
		bool isValid() const;

		QString function;
		QString kernelCode;
		OCLDevice device;
		QString programLocation;
		QString macroString;

	private:

		template<typename FIRST>
		static ExecutionParameters toParams(FIRST first)
		{
			ExecutionParameters list;
			list.push_back(first.param);
			return list;
		}

		template<typename FIRST, typename ...REST>
		static ExecutionParameters toParams(FIRST first, REST...rest)
		{
			ExecutionParameters list = toParams<REST...>(rest...);
			list.push_back(first.param);
			return list;
		}

		void* m_executable = nullptr;
		bool m_moved = false;
	};
}
