//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "IntegrationFunctions.h"
#include "DNP_CompilationEngine.h"
#include "DNP_Executable.h"
#include "ExecutionParameters.h"
#include "DNP_ExecutionEngine.h"

int EasyOCL::get_global_id(int dimension) { throw std::runtime_error("do not use in cpp"); }
