//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "DNP_CompilationEngine.h"
#include "DNP_ErrorEngine.h"
#include "FunctionalMacros.h"

using namespace EasyOCL;
using namespace cl;

EasyOCL::DNP_Executable EasyOCL::DNP_CompilationEngine::compileProgram(const QString& source,
                                             const QString& function,
                                             cl::Device device,
                                             const QString& _constants /*= ""*/)
{
   QString constants = _constants;
	DNP_Executable result;
   cl_int error;

   result.source = source;
   result.device = device;
   result.function = function;

   result.context = Context(result.device, NULL, NULL, NULL, &error);
   if (checkError(result, OCL_CURRENT_LOCATION, error)) return result;

   result.program = Program(result.context, source.toStdString(), &error);
   if (checkError(result, OCL_CURRENT_LOCATION, error)) return result;
   
   std::string res = constants.toStdString();
   const char* _res = res.c_str();
   error = result.program.build({device}, _res);
   if (checkError(result, OCL_CURRENT_LOCATION, error)) return result;

   result.kernel = Kernel(result.program, result.function.toStdString().c_str(), &error);
   if (checkError(result, OCL_CURRENT_LOCATION, error)) return result;

   result.queue = CommandQueue(result.context, result.device, NULL, &error);
   if (checkError(result, OCL_CURRENT_LOCATION, error)) return result;

   return result;
}

bool EasyOCL::DNP_CompilationEngine::checkError(DNP_Executable& exec, const QString& location, cl_int error)
{
   if (error == CL_SUCCESS)
      return false;

   if (error == CL_BUILD_PROGRAM_FAILURE)
   {
      exec.error = getBuildLog(exec.program, exec.device);
      exec.errorLocation = location;
      exec.errorType = ErrorType::BUILD_ERROR;
   } else
   {
      exec.error = getErrorMessage(error);
      exec.errorLocation = location;
      exec.errorType = ErrorType::OTHER_ERROR;
   }

   return true;
}



QString EasyOCL::DNP_CompilationEngine::getBuildLog(const cl::Program& prgm, cl::Device device)
{
   return QString::fromStdString(prgm.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device));
}
