//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "OCLDevice.h"
#include "ExecutionResult.h"

namespace EasyOCL
{
	template<typename T, typename ...Args>
	inline EasyOCL::ExecutionResult execute(const OCLDevice& device, std::size_t iterations, const QString& fn, Args...args)
	{
		return T::__class_programs[fn][device](iterations, args...);
	}
}