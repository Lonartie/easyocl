//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

/************************************************************************/
/*         INCLUDE ONLY IN EASYOCL PROJECT - EXPOSED CL HEADER!         */
/************************************************************************/

#include "stdafx.h"
#include "ErrorType.h"
#include "ErrorClass.h"
#include <CL/cl.hpp>

namespace EasyOCL
{
   struct DNP_Executable : public ErrorClass
   {
      std::vector<cl::Buffer> buffers;
      cl::Kernel kernel;
      cl::CommandQueue queue;
      cl::Program program;
      cl::Context context;
      cl::Device device;
      QString source;
      QString function;
      std::size_t iterations;
   };
}