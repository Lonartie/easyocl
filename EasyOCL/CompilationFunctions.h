//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "OCLDevice.h"
#include "ExecutionResult.h"
#include "KernelClass.h"

namespace EasyOCL
{

   template<typename T>
   bool __compile_function(const QString& fn, const OCLDevice& device)
   {
      auto& smap = T::__class_functions;
      auto& pmap = T::__class_programs;
      auto& imap = T::__function_includes;
      auto& lmap = T::__function_locations;
      auto& mmap = T::__macros;
      auto& ifmap = T::__include_functions;

      auto fn_exists = false;

      for (const auto& fnp : pmap)
      {
         if (fnp.first == fn)
         {
            fn_exists = true;
            for (const auto& dvp : fnp.second)
            {
               if (dvp.first == device)
               {
                  return true;
               }
            }
         }
      }

      if (ifmap.size() > 0) // if includes exist
      {
         if (std::find_if(ifmap.begin(), ifmap.end(), [&](auto& kvp) { return kvp.first == fn; }) != ifmap.end()) // if includes for this function exist
         {
            // if result map doesn't exist, create one
            if (std::find_if(imap.begin(), imap.end(), [&](auto& kvp) { return kvp.first == fn; }) != imap.end())
            {
               imap.emplace(fn, std::vector<QString>());
            }

            // if not already in result map, copy 
            for (const auto& _if : ifmap[fn])
            {
               auto cont = _if();
               bool exists = false;

               for (const auto& res : imap[fn])
               {
                  if (cont == res)
                  {
                     exists = true;
                  }
               }

               if (!exists)
               {
                  imap[fn].push_back(cont);
               }
            }
         }
      }

      auto source = smap[fn];
      auto location = lmap[fn];

      if (std::find_if(imap.begin(), imap.end(), [&](auto& kvp) { return kvp.first == fn; }) != imap.end())
      {
         for (const auto& inc : imap[fn])
         {
            source.prepend(inc + "\n");
         }
      }

      QString macro_string = "";

      if (std::find_if(mmap.begin(), mmap.end(), [&](auto& kvp) { return kvp.first == fn; }) != mmap.end())
      {
         for (const auto& mac : mmap[fn])
         {
            macro_string.prepend(mac.trimmed() + QString("; "));
            QString definition = "#define " + mac + "\n";
            definition.replace(definition.indexOf("="), 1, " ");
            source.prepend(definition);
         }
      }

      // enabling debugging support with printf
      //source.prepend("#pragma OPENCL EXTENSION cl_amd_printf : enable\n");
      //source.prepend("#pragma OPENCL EXTENSION cl_intel_printf : enable\n");

      macro_string.remove(macro_string.lastIndexOf(";"), 1);

      auto prgm = KernelClass::__compile(source, fn, device, location);
      prgm.macroString = macro_string;

      if (fn_exists)
      {
         pmap[fn].emplace(device, std::move(prgm));
      } else
      {
         std::map<OCLDevice, OCLProgram> _map;
         _map.emplace(device, std::move(prgm));
         pmap.emplace(fn, std::move(_map));
      }

      return prgm.isValid();
   }

   template<typename CLS, typename ...ARGS>
   bool compile(EasyOCL::ExecutionResult(*fn_pointer)(ARGS...), const OCLDevice& device)
   {
      auto _hash = FUNCTION_HASH(fn_pointer);
      auto& fn = CLS::__class_pointers[_hash];
      return __compile_function<CLS>(fn, device);
   }

   template<typename CLS, typename ...ARGS>
   bool compile(EasyOCL::ExecutionResult(*fn_pointer)(ARGS...))
   {
      bool result = true;

      for (const auto& device : OCLDevice::getDevices())
      {
         auto _hash = FUNCTION_HASH(fn_pointer);
         auto& fn = CLS::__class_pointers[_hash];
         result = result && __compile_function<CLS>(fn, device);
      }

      return result;
   }

   template<typename CLS>
   bool compile(const QString& fn)
   {
      bool result = true;

      for (const auto& device : OCLDevice::getDevices())
      {
         result = result && __compile_function<CLS>(fn, device);
      }

      return result;
   }

   template<typename CLS>
   bool compile(const QString& fn, const OCLDevice& device)
   {
      return __compile_function<CLS>(fn, device);
   }
}