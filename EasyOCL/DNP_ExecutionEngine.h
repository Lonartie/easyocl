//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

/************************************************************************/
/*         INCLUDE ONLY IN EASYOCL PROJECT - EXPOSED CL HEADER!         */
/************************************************************************/

#include "stdafx.h"
#include "DNP_Executable.h"
#include "ExecutionResult.h"
#include <CL/cl.hpp>

namespace EasyOCL
{
   class DNP_ExecutionEngine
   {
   public:

      static ExecutionResult execute(DNP_Executable& exec, ExecutionParameters& params);

   private:
      static cl::Buffer createBuffer(ExecutionResult& res, const cl::Context& context, ExecutionParameter& param);
      static bool writeData(ExecutionResult& result, DNP_Executable& exec, ExecutionParameters& params);
      static bool run(ExecutionResult& result, DNP_Executable& exec);
      static bool readData(ExecutionResult& result, DNP_Executable& exec, ExecutionParameters& params);

   };
}