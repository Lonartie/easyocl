//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "OCLDevice.h"

#include <CL/cl.hpp>

namespace
{
	std::vector<cl::Device> All()
	{
		std::vector<cl::Platform> platforms;
		std::vector<cl::Device> alldevices;
		std::vector<cl::Device> devices;
		cl::Platform::get(&platforms);

		for (cl::Platform& platform : platforms)
		{
			platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
			alldevices.insert(alldevices.end(), devices.begin(), devices.end());
			devices.clear();
		}

		return alldevices;
	}

	cl::Device MainDefault()
	{
		return All()[0];
	}

	cl::Device MainCPU()
	{
		for (const auto& device : All())
			if (device.getInfo<CL_DEVICE_TYPE>() == CL_DEVICE_TYPE_CPU)
				return device;
		return MainDefault();
	}

	cl::Device MainGPU()
	{
		for (const auto& device : All())
			if (device.getInfo<CL_DEVICE_TYPE>() == CL_DEVICE_TYPE_GPU)
				return device;
		return MainDefault();
	}
}

EasyOCL::OCLDevice EasyOCL::OCLDevice::GPU()
{
	return OCLDevice(getGPU());
}

EasyOCL::OCLDevice EasyOCL::OCLDevice::CPU()
{
	return OCLDevice(getCPU());
}

std::vector<EasyOCL::OCLDevice> EasyOCL::OCLDevice::getDevices()
{
	std::vector<OCLDevice> devices;
	for (auto& device : All())
		devices.push_back(OCLDevice(new cl::Device(device)));
	return devices;
}

EasyOCL::OCLDevice::OCLDevice(const OCLDevice& o)
	: m_data(new cl::Device(*static_cast<const cl::Device*>(o.m_data)))
{
}

EasyOCL::OCLDevice::OCLDevice(OCLDevice&& o)
	: m_data(new cl::Device(*static_cast<const cl::Device*>(o.m_data)))
{
}

EasyOCL::OCLDevice::OCLDevice(void* data)
	: m_data(data)
{
}

EasyOCL::OCLDevice::~OCLDevice()
{
	cl::Device* ptr = static_cast<cl::Device*>(m_data);
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
		m_data = nullptr;
	}
}

const void* EasyOCL::OCLDevice::data() const
{
	return m_data;
}

bool EasyOCL::OCLDevice::operator==(const OCLDevice& o) const noexcept
{
   return getName() == o.getName();
}

bool EasyOCL::OCLDevice::operator!=(const OCLDevice& o) const noexcept
{
	return !operator==(o);
}

bool EasyOCL::OCLDevice::operator<(const OCLDevice& o) const noexcept
{
	return getName() < o.getName();
}

EasyOCL::OCLDevice& EasyOCL::OCLDevice::operator=(const OCLDevice& o)
{
	cl::Device* ptr = static_cast<cl::Device*>(m_data);
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
		m_data = nullptr;
	}

	m_data = new cl::Device(*static_cast<cl::Device*>(o.m_data));
	return *this;
}

EasyOCL::OCLDevice& EasyOCL::OCLDevice::operator=(OCLDevice&& o)
{
	cl::Device* ptr = static_cast<cl::Device*>(m_data);
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
		m_data = nullptr;
	}

	m_data = new cl::Device(*static_cast<cl::Device*>(o.m_data));
	return *this;
}

QString EasyOCL::OCLDevice::getName() const
{
	if (m_name.isEmpty())
   {
      auto& device = *static_cast<cl::Device*>(m_data);
      return QString::fromStdString(device.getInfo<CL_DEVICE_NAME>());
	}

	return m_name;
}

QString EasyOCL::OCLDevice::getName()
{
   if (m_name.isEmpty())
   {
      auto& device = *static_cast<cl::Device*>(m_data);
      m_name = QString::fromStdString(device.getInfo<CL_DEVICE_NAME>());
   }

   return m_name;
}

EasyOCL::DeviceType EasyOCL::OCLDevice::getDeviceType() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
	auto type = device.getInfo<CL_DEVICE_TYPE>();
	return
      type == CL_DEVICE_TYPE_CPU ? DeviceType::CPU :
		type == CL_DEVICE_TYPE_GPU ? DeviceType::GPU :
		DeviceType::Others;
}

EasyOCL::Vendor EasyOCL::OCLDevice::getVendor() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
	auto vendor = QString::fromStdString(device.getInfo<CL_DEVICE_VENDOR>());

	return
		vendor.contains("intel", Qt::CaseInsensitive) ? Vendor::Intel :
		vendor.contains("nvidia", Qt::CaseInsensitive) ? Vendor::NVidia :
		vendor.contains("amd", Qt::CaseInsensitive) ? Vendor::AMD :
		Vendor::Unknown;
}

std::size_t EasyOCL::OCLDevice::getClockFrequency() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
	return device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>();
}

std::size_t EasyOCL::OCLDevice::getAvailableMemory() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
	return device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
}

std::size_t EasyOCL::OCLDevice::getWorkGroupSize() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
	return device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
}

std::size_t EasyOCL::OCLDevice::getComputeUnits() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
	return device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
}

QString EasyOCL::OCLDevice::getDriverVersion() const
{
   auto& device = *static_cast<cl::Device*>(m_data);
   return QString::fromStdString(device.getInfo<CL_DRIVER_VERSION>());
}

void* EasyOCL::OCLDevice::getGPU()
{
	return new cl::Device(MainGPU());
}

void* EasyOCL::OCLDevice::getCPU()
{
	return new cl::Device(MainCPU());
}
