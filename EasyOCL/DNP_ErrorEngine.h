//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

/************************************************************************/
/*         INCLUDE ONLY IN EASYOCL PROJECT - EXPOSED CL HEADER!         */
/************************************************************************/

#include "stdafx.h"
#include <CL/cl.hpp>

namespace EasyOCL
{
   QString getErrorMessage(cl_int error);
}