//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "DNP_ExecutionEngine.h"
#include "DNP_ErrorEngine.h"
#include "FunctionalMacros.h"

using namespace EasyOCL;
using namespace cl;

ExecutionResult DNP_ExecutionEngine::execute(DNP_Executable& exec, ExecutionParameters& params)
{
	ExecutionResult result;
	result.data = params;

	if (!writeData(result, exec, params)) return result;

	if (!run(result, exec)) return result;

	readData(result, exec, params);

	return result;

}

bool DNP_ExecutionEngine::writeData(ExecutionResult& result, DNP_Executable& exec, ExecutionParameters& params)
{
	cl_int error = CL_SUCCESS;

	auto new_buffer = exec.buffers.size() == 0;

	for (std::size_t i = 0; i < params.size(); i++)
	{
		auto& param = params[i];
		const void* data = nullptr;

		if (param.accessType == READ) data = param.r_data;
		else if (param.accessType == WRITE) data = param.w_data;
		else if (param.accessType == READ_WRITE) data = param.rw_data;

		if (!data) continue; // maybe intentionally -> do not copy buffer because of performance!

		if (new_buffer) exec.buffers.push_back(createBuffer(result, exec.context, param));
		else exec.buffers[i] = createBuffer(result, exec.context, param);

		if (result.errorType != ErrorType::NO_CL_ERROR) return false;

		error = exec.queue.enqueueWriteBuffer(exec.buffers[i], true, 0, param.dataSize * param.byteSize, data);

		if (error != CL_SUCCESS)
		{
			result.error = getErrorMessage(error);
			result.errorLocation = OCL_CURRENT_LOCATION;
			result.errorType = ErrorType::WRITE_BUFFER_ERROR;
			return false;
		}

		error = exec.kernel.setArg(i, exec.buffers[i]);

		if (error != CL_SUCCESS)
		{
			result.error = getErrorMessage(error);
			result.errorLocation = OCL_CURRENT_LOCATION;
			result.errorType = ErrorType::WRITE_BUFFER_ERROR;
			return false;
		}
	}

	return true;
}

bool DNP_ExecutionEngine::run(ExecutionResult& result, DNP_Executable& exec)
{
	cl_int error = CL_SUCCESS;

	error = exec.queue.enqueueNDRangeKernel(exec.kernel, cl::NullRange, cl::NDRange(exec.iterations));

	if (error != CL_SUCCESS)
	{
		result.error = getErrorMessage(error);
		result.errorLocation = OCL_CURRENT_LOCATION;
		result.errorType = ErrorType::RUNTIME_ERROR;
		return false;
	}

	error = exec.queue.finish();

	if (error != CL_SUCCESS)
	{
		result.error = getErrorMessage(error);
		result.errorLocation = OCL_CURRENT_LOCATION;
		result.errorType = ErrorType::RUNTIME_ERROR;
		return false;
	}

	return true;
}

bool DNP_ExecutionEngine::readData(ExecutionResult& result, DNP_Executable& exec, ExecutionParameters& params)
{
	cl_int error = CL_SUCCESS;

	for (std::size_t i = 0; i < params.size(); i++)
	{
		auto& param = params[i];
		if (param.accessType == READ) continue;

		void* data =
			param.accessType == WRITE ? param.w_data :
			param.accessType == READ_WRITE ? param.rw_data :
			nullptr;

		if (!data) continue; // maybe intentionally -> do not copy buffer because of performance!

		error = exec.queue.enqueueReadBuffer(exec.buffers[i], true, 0, param.byteSize * param.dataSize, data);

		if (error != CL_SUCCESS)
		{
			result.error = getErrorMessage(error);
			result.errorLocation = OCL_CURRENT_LOCATION;
			result.errorType = ErrorType::RUNTIME_ERROR;
			return false;
		}
	}

	return true;
}

Buffer DNP_ExecutionEngine::createBuffer(ExecutionResult& res, const Context& context, ExecutionParameter& param)
{
	cl_int error = CL_SUCCESS;

	int flag =
		param.accessType == READ ? CL_MEM_READ_ONLY :
		param.accessType == WRITE ? CL_MEM_WRITE_ONLY :
		param.accessType == READ_WRITE ? CL_MEM_READ_WRITE :
		-1;

	if (flag == -1)
	{
		res.error = "CL_MEM_TYPE_NOT_KNOWN";
		res.errorLocation = OCL_CURRENT_LOCATION;
		res.errorType = ErrorType::BUFFER_CREATION_ERROR;
		return Buffer();
	}

	Buffer buf = Buffer(context, flag, param.byteSize * param.dataSize, NULL, &error);

	if (error != CL_SUCCESS)
	{
		res.error = getErrorMessage(error);
		res.errorLocation = OCL_CURRENT_LOCATION;
		res.errorType = ErrorType::BUFFER_CREATION_ERROR;
		return Buffer();
	}

	return buf;
}
