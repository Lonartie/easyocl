//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "FormattingFunctions.h"

namespace EasyOCL
{
	QString __format(const QString& fn, bool is_func)
	{
      QString all = fn;

      all.replace("\n", "");
      all.replace("\r", "");

      all.replace("{", "\n{\n");

		if (is_func)
		{
			auto first_line = all.split("\n")[0];
			auto formatted_first_line = first_line;
			QStringList list = formatted_first_line.split("(")[1].split(",");
			for (auto& arg : list)
			{
				auto _arg = arg.split(")")[0];
				if (_arg.contains("*"))
				{
					formatted_first_line.replace(_arg, "global " + _arg);
				}
			}
			formatted_first_line.replace("const", "");
			all.replace(first_line, formatted_first_line);
      }

      all.replace("}", "\n}\n");
		all.replace(";", ";\n");
		all.replace("   ", " ");
		all.replace("  ", " ");
		all.replace("//", "\n//");
		all.replace("/*", "\n/*");
		all.replace("*/", "*/\n");
		all.replace("#", "\n#");
		all.replace("kernel ", "\nkernel ");

		all.replace(QRegularExpression("\\w+::"), "");

		return all;
	}
}