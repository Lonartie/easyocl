//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "ErrorType.h"

namespace EasyOCL
{
   struct ErrorClass
   {
      QString error;
      QString errorLocation;
      ErrorType errorType = ErrorType::NO_CL_ERROR;
   };
}