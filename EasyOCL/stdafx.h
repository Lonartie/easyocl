//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "easyocl_global.h"

// STL
#include <vector>
#include <map>

// Qt
#include <QtCore/QtCore>

#include <windows.h>

namespace EasyOCL
{
   static inline void Console()
   {
      AllocConsole();
      FILE* pFileCon = NULL;
      pFileCon = freopen("CONOUT$", "w", stdout);

      COORD coordInfo;
      coordInfo.X = 130;
      coordInfo.Y = 9000;

      SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coordInfo);
      SetConsoleMode(GetStdHandle(STD_OUTPUT_HANDLE), ENABLE_QUICK_EDIT_MODE | ENABLE_EXTENDED_FLAGS);
   }
}