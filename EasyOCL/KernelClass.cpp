//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#include "stdafx.h"
#include "KernelClass.h"
#include "DNP_CompilationEngine.h"
#include "FormattingFunctions.h"

EasyOCL::OCLProgram EasyOCL::KernelClass::__compile(const QString& source,
																	 const QString& fn,
																	 const OCLDevice& device,
																	 const QString& location,
                                                    const QString& macros)
{
	auto formatted_source = __format(source, false);
	OCLProgram prgm(new DNP_Executable(DNP_CompilationEngine::compileProgram(formatted_source, fn, *static_cast<const cl::Device*>(device.data()), macros)));

	prgm.device = device;
	prgm.function = fn;
	prgm.kernelCode = formatted_source;
	prgm.programLocation = location;
	prgm.macroString = macros;

	return prgm;
}
