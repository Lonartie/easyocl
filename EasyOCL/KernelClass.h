//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "OCLDevice.h"
#include "OCLProgram.h"
#include "KernelClassMacros.h"
#include "CompilationFunctions.h"

namespace EasyOCL
{
	class EASYOCL_EXPORT KernelClass
	{
	public:

		static OCLProgram __compile(const QString& source,
											 const QString& fn,
											 const OCLDevice& device,
											 const QString& location,
											 const QString& macros = "");

	};
}
