//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

// $DN_PUBLISH

/************************************************************************/
/*         INCLUDE ONLY IN EASYOCL PROJECT - EXPOSED CL HEADER!         */
/************************************************************************/

#include "stdafx.h"
#include "DNP_Executable.h"

#include <CL/cl.hpp>

namespace EasyOCL
{
   class DNP_CompilationEngine
   {
   public:
      static DNP_Executable compileProgram(const QString& source, const QString& function, cl::Device device, const QString& constants = "");

   private:
      static bool checkError(DNP_Executable& exec, const QString& location, cl_int error);
      static QString getBuildLog(const cl::Program& prgm, cl::Device device);

   };
}